# scarab
scarab: a relay board for the Raspberry Pico W

![scarab](images/scarab.png?raw=true "scarab")

## What?

scarab is a shield for the Raspberry Pico W that uses MQTT to control eight relays and eight boolean inputs.

It is ideally suited as a hydroponic control system.

scarab is based on Franco "nextime" Lanza's [OSSO](https://git.nexlab.net/nexlab/Osso).

## Features?

* The scarab provides Raspberry Pi-pinout-compatible I2C headers for an I2C bus.
* +12VDC, +5VDC (3A), and +3.3VDC (1A) are exposed via a Euroblock header.
* The scarab relays and inputs are controlled through an MQTT topic
* The scarab configuration is through text files on a microSD card

## Improvements over OSSO?

* The dueling +5VDC regulators have been replaced with a single 3A LDO regulator.
* +3.3VDC at 1A is generated through a switching regulator, as the BeagleBone's onboard 250mA regulator is inadequate to power several I2C devices.
* correct bypass capacitors have been placed on all regulated voltage rails.
* Ground fills are now used.
* the BeagleBone has been replaced with a Raspberry Pico W


## Why?

Back when I worked for the United Fruit Company, I used the OSSO as the control system for a robotic prototype.  When that project was cancelled, I repurposed the OSSO as the control system for my basement hydroponic garden.

Ten years later ... I am now retired, and have enough time to work on personal projects.  I converted the Eagle project files to KiCAD, addressed shortcomings in the original implementation, and added the ability to easily use standard I2C modules.

A year after that ... I grew frustrated with the needlessly complex moving target that is Linux on ARM, and reworked the board to use a Raspberry Pico W.

## How?

The KiCAD project is in the KiCAD subdirectory.  Generation of Gerber files for your favorite fab should be straightforward.

## Documentation?

Mostly forthcoming, but the executive summary of configuration / MQTT interfacing is:

* the microSD card should be formatted as FAT32,
* the SSID is specified in a file named "ssid",
* wireless authentication secret is specified in a file named "auth",
* MQTT server is specified in a file named "mqtt",
* MQTT topic is "scarab/${PICO_CLIENT_ID/",
* relays are controlled by publishing "on" or "off" to "scarab/${PICO_CLIENT_ID/command/relay[0-7]",
* relay status is pushed to "scarab/${PICO_CLIENT_ID/status/relay[0-7]",
* input status is pushed to "scarab/${PICO_CLIENT_ID/status/input[0-7]"

## Who?

I'm Chris Ryu.  I'm a retired engineer.  I can be contacted at <software-ryucats@disavowed.jp>

## License?

The scarab is licensed under the GNU Public License, version 3 (at the time of writing).  If you take the design, modify it, and make it available ... please give credit where credit's due.
