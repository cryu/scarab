from usocket import socket
import ustruct as struct
import machine
from machine import Pin, SPI, I2C
import network
import time
import ubinascii
from umqtt import MQTTClient
import ntptime
import sdcard
import uos

class ScarabBoard(object):
    client_id = ubinascii.hexlify(machine.unique_id())
    scarab_relay = [None] * 8
    scarab_input = [None] * 8
    nic = None
    ssid = None
    auth = None
    mqtt_client = None

    def __init__(self):
#        self.i2c = I2C(sda = Pin(26), scl = Pin(27))
        self.scarab_relay[0] = Pin(7, mode = Pin.OUT, value=0)
        self.scarab_relay[1] = Pin(6, mode = Pin.OUT, value=0)
        self.scarab_relay[2] = Pin(5, mode = Pin.OUT, value=0)
        self.scarab_relay[3] = Pin(4, mode = Pin.OUT, value=0)
        self.scarab_relay[4] = Pin(3, mode = Pin.OUT, value=0)
        self.scarab_relay[5] = Pin(2, mode = Pin.OUT, value=0)
        self.scarab_relay[6] = Pin(1, mode = Pin.OUT, value=0)
        self.scarab_relay[7] = Pin(0, mode = Pin.OUT, value=0)

        self.scarab_input[0] = Pin(12, mode = Pin.IN, pull = Pin.PULL_UP)
        self.scarab_input[1] = Pin(13, mode = Pin.IN, pull = Pin.PULL_UP)
        self.scarab_input[2] = Pin(14, mode = Pin.IN, pull = Pin.PULL_UP)
        self.scarab_input[3] = Pin(15, mode = Pin.IN, pull = Pin.PULL_UP)
        self.scarab_input[4] = Pin(11, mode = Pin.IN, pull = Pin.PULL_UP)
        self.scarab_input[5] = Pin(10, mode = Pin.IN, pull = Pin.PULL_UP)
        self.scarab_input[6] = Pin(9, mode = Pin.IN, pull = Pin.PULL_UP)
        self.scarab_input[7] = Pin(8, mode = Pin.IN, pull = Pin.PULL_UP)
    def __del__(self):
        for i in range(0,8):
            self.relay_off(i)
    def relay(self, unit, state):
        self.scarab_relay[unit].value(state)
    def relay_on(self, unit):
        print('turning relay {} on'.format(unit))
        self.scarab_relay[unit].value(1)
    def relay_off(self, unit):
        print('turning relay {} off'.format(unit))
        self.scarab_relay[unit].value(0)
    def input_status(self, unit):
        return not (self.scarab_input[unit].value())
    def relay_status(self, unit):
        return bool(self.scarab_relay[unit].value())
    def nic_init(self):
        network.country('JP')
        network.hostname('scarab-'+self.client_id.decode())
        print('hostname: {}'.format(network.hostname()))
        self.nic = network.WLAN(network.STA_IF)
        self.nic.active(True)
        self.nic.config(pm = 0xa11140)
        aps = self.nic.scan()
        while len(aps) is 0:
            print('no APs?')
            time.sleep(5)
            aps = self.nic.scan()
        print(aps)
        self.nic.connect(self.ssid, self.auth)
        while not self.nic.isconnected() and self.nic.status() >= 0:
            print('waiting to connect {}'.format(self.nic.status()))
            time.sleep(1)
        print('Network configured: {}'.format(self.nic.ifconfig()))

    def nic_check(self):
        if not self.nic.isconnected():
            print('disconnected?')
            self.nic.connect(self.ssid, self.auth)
            while not self.nic.isconnected() and self.nic.status() >= 0:
                print('waiting to connect {}'.format(self.nic.status()))
                time.sleep(1)
                print('Network configured: {}'.format(self.nic.ifconfig()))

    def mqtt_callback(self, topic, msg):
        msg = msg.lower().decode()
        print((topic, msg))
        relay = topic.decode().split('/')[-1]
        if relay[0:5] != 'relay':
            print('bogus gunk: {}'.format(relay))
            return
        frob = int(relay[-1])
        if msg is 'on':
            self.relay_on(frob)
        else:
            self.relay_off(frob)
    def sdcard_init(self):
        cs = Pin(17, Pin.OUT)
        spi = SPI(0, baudrate=1000000, polarity=0, phase=0, bits=8, firstbit=SPI.MSB, sck=Pin(18), mosi=Pin(19), miso=Pin(16))
        self.sd = sdcard.SDCard(spi, cs)

    def mqtt_init(self):
        topic_sub = b'scarab/'+self.client_id+'/command/#'
        self.mqtt_client = MQTTClient(self.client_id, self.mqtt_server, keepalive=3600)
        self.mqtt_client.set_callback(self.mqtt_callback)
        self.mqtt_client.connect()
        self.mqtt_client.subscribe(topic_sub)
        print('Connected to {} MQTT broker, subscribed to {} topic'.format(self.mqtt_server, topic_sub.decode()))

    def sdcard_config(self):
        try:
            self.sdcard_init()
        except:
            print('could not init SD card')
            return
        vfs = uos.VfsFat(self.sd)
        uos.mount(vfs, '/sd')
        print()
        print('reading config from sdcard')
        try:
            with open("/sd/ssid", "r") as file:
                self.ssid = file.read().rstrip(' \t\n\r')
                print('SSID: {}'.format(self.ssid))
            file.close()
        except:
            print('could not read ssid')
            return
        try:
            with open("/sd/auth", "r") as file:
                self.auth = file.read().rstrip(' \t\n\r')
                print('auth: {}'.format(self.auth))
            file.close()
        except:
            print('could not read auth')
            return
        try:
            with open("/sd/mqtt", "r") as file:
                self.mqtt_server = file.read().rstrip(' \t\n\r')
                print('MQTT server: {}'.format(self.mqtt_server))
            file.close()
        except:
            print('could not read mqtt')
            return
