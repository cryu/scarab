from usocket import socket
import ustruct as struct
import machine
from machine import Pin, SPI, I2C
import network
import time
import ubinascii
from umqtt import MQTTClient
import ntptime
import rp2
import sdcard
import uos

from scarab import ScarabBoard

def main():
    last_message = 0
    message_interval = 5
    counter = 0

    led = Pin("LED", Pin.OUT)

    # LED on, we're executing main()
    led.on()

    print('scarab v0.98')
    scarab = ScarabBoard()

    # wait a few seconds to let the microSD card power up
    time.sleep(5)

    # LED off, we're reading config from microSD
    led.off()
    scarab.sdcard_config()

    # LED on, we're initializing the NIC
    led.on()
    scarab.nic_init()

    # LED off, we're initializing MQTT

    led.off()
    time.sleep(1)
    topic_pub = b'scarab/'+scarab.client_id+'/status'
    scarab.mqtt_init()

    print('is initialized')

    while True:
        led.on()
        scarab.nic_check()
        scarab.mqtt_client.check_msg()
        msg = b'Hello #%d' % counter
        scarab.mqtt_client.publish(topic_pub, msg)
        last_message = time.time()
        counter += 1

        for j in range(0,8):
            status = scarab.input_status(j)
            scarab.mqtt_client.publish(topic_pub+'/input{}'.format(j), str(status))
            status = scarab.relay_status(j)
            scarab.mqtt_client.publish(topic_pub+'/relay{}'.format(j), str(status))

        led.off()
        time.sleep(1)

if __name__ == "__main__":
    main()
